import React, { useState } from "react";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import uuid from "uuid/v4";
import { Badge, Button, Col, Row, InputGroup, FormControl } from 'react-bootstrap'
import {Filter as FilterIcon, ArrowRepeat as RefreshIcon, Search as SearchIcon, GearFill as SettingsIcon } from 'react-bootstrap-icons'
import './App.css'

const orderDetails = (serialNo, orderNo, orderAddress, orderDue) => {

  return (
    <div>
      <div><span className="pr-10">#{serialNo}</span><Badge variant="info" className="bg-primary" bg="primary"> Response due</Badge></div>
      <div className="orderNo">Order No: #{orderNo}</div>
      <div className="orderAddress">{orderAddress}</div>
      <div className="orderDue">
        <div>DUE: {orderDue}</div>
        <div>ASSIGNED TO</div>
        <div className="round">&nbsp;</div>
      </div>
    </div>
  )
}

const itemsFromBackend = [
  { 
    id: uuid(),
    content: orderDetails(21, 45, 'Paratha Side Dish (2 Nos), Plain Dosa, Podi Ghee Dosa (1 Nos), Butter Roti (5 Nos)', 'Aug 10, 2021 03:39 PM')
  },
  { 
    id: uuid(),
    content: orderDetails(22, 201, 'Paratha Side Dish (2 Nos), Plain Dosa, Podi Ghee Dosa (1 Nos), Butter Roti (5 Nos)', 'Aug 10, 2021 03:39 PM')
  },
  { 
    id: uuid(),
    content: orderDetails(23, 202, 'Paratha Side Dish (2 Nos), Plain Dosa, Podi Ghee Dosa (1 Nos), Butter Roti (5 Nos)', 'Aug 10, 2021 03:39 PM')
  },
  { 
    id: uuid(),
    content: orderDetails(24, 210, 'Paratha Side Dish (2 Nos), Plain Dosa, Podi Ghee Dosa (1 Nos), Butter Roti (5 Nos)', 'Aug 10, 2021 03:39 PM')
  },
  { 
    id: uuid(),
    content: orderDetails(25, 37, 'Paratha Side Dish (2 Nos), Plain Dosa, Podi Ghee Dosa (1 Nos), Butter Roti (5 Nos)', 'Aug 10, 2021 03:39 PM')
  }
];

const columnsFromBackend = {
  [uuid()]: {
    name: `RECEIVED ORDERS`,
    items: itemsFromBackend
  },
  [uuid()]: {
    name: "ORDER IN PROGRESS",
    items: []
  },
  [uuid()]: {
    name: "ORDER IS READY FOR DELIVERY",
    items: []
  },
  [uuid()]: {
    name: "ORDER PICKED UP",
    items: []
  }
};

const onDragEnd = (result, columns, setColumns) => {
  if (!result.destination) return;
  const { source, destination } = result;

  if (source.droppableId !== destination.droppableId) {
    const sourceColumn = columns[source.droppableId];
    const destColumn = columns[destination.droppableId];
    const sourceItems = [...sourceColumn.items];
    const destItems = [...destColumn.items];
    const [removed] = sourceItems.splice(source.index, 1);
    destItems.splice(destination.index, 0, removed);
    setColumns({
      ...columns,
      [source.droppableId]: {
        ...sourceColumn,
        items: sourceItems
      },
      [destination.droppableId]: {
        ...destColumn,
        items: destItems
      }
    });
  } else {
    const column = columns[source.droppableId];
    const copiedItems = [...column.items];
    const [removed] = copiedItems.splice(source.index, 1);
    copiedItems.splice(destination.index, 0, removed);
    setColumns({
      ...columns,
      [source.droppableId]: {
        ...column,
        items: copiedItems
      }
    });
  }
};

function App() {
  const [columns, setColumns] = useState(columnsFromBackend);
  return (
    <div>
      <div className="p-10">
        <Row>
          <Col lg="6">
          <Row className="topBar">
            <h3>Tickets</h3>
            <Button variant="success">All</Button>
            <Button variant="secondary">Only My Tickets</Button>
            <Button variant="secondary">Recently Updated</Button>
            <Button variant="secondary"><FilterIcon /></Button>
            <Button variant="secondary"><RefreshIcon /></Button>
            </Row>
          </Col>
          <Col lg="2"></Col>
          <Col lg="2" className="p-0">
            <Row className="topBar">
              <InputGroup>
                <FormControl placeholder="Search" />
                <Button variant="secondary"><SearchIcon /></Button>
              </InputGroup>
            </Row>
          </Col>
          <Col lg="2" className="p-0">
            <Row className="topBar">
              <Button variant="secondary"><SettingsIcon /> Configurations</Button>
            </Row>
          </Col>
        </Row>
      </div>
      <div style={{ display: "flex", justifyContent: "center", height: "100%" }}>
        <DragDropContext
          onDragEnd={result => onDragEnd(result, columns, setColumns)}
        >
          {Object.entries(columns).map(([columnId, column], index) => {
            return (
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center"
                }}
                key={columnId}
              >
                <h5>{column.name} ({column.items.length})</h5>
                <div style={{ margin: 8 }}>
                  <Droppable droppableId={columnId} key={columnId}>
                    {(provided, snapshot) => {
                      return (
                        <div className='dragContainer'
                          {...provided.droppableProps}
                          ref={provided.innerRef}
                          style={{
                            background: snapshot.isDraggingOver
                              ? "lightblue"
                              : "lightgrey",
                            padding: 4,
                            width: 290,
                            minHeight: 500
                          }}
                        >
                          {column.items.map((item, index) => {
                            return (
                              <Draggable
                                key={item.id}
                                draggableId={item.id}
                                index={index}
                              >
                                {(provided, snapshot) => {
                                  return (
                                    <div
                                      ref={provided.innerRef}
                                      {...provided.draggableProps}
                                      {...provided.dragHandleProps}
                                      style={{
                                        userSelect: "none",
                                        padding: 16,
                                        margin: "0 0 8px 0",
                                        minHeight: "50px",
                                        backgroundColor: snapshot.isDragging
                                          ? "#EEE"
                                          : "#FFF",
                                        ...provided.draggableProps.style
                                      }}
                                    >
                                      {item.content}
                                    </div>
                                  );
                                }}
                              </Draggable>
                            );
                          })}
                          {provided.placeholder}
                        </div>
                      );
                    }}
                  </Droppable>
                </div>
              </div>
            );
          })}
        </DragDropContext>
      </div>
    </div>
  );
}

export default App;
